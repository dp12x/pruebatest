import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelHComponent } from './panel-h.component';

describe('PanelHComponent', () => {
  let component: PanelHComponent;
  let fixture: ComponentFixture<PanelHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelHComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
