import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelVComponent } from './panel-v.component';

describe('PanelVComponent', () => {
  let component: PanelVComponent;
  let fixture: ComponentFixture<PanelVComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelVComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
