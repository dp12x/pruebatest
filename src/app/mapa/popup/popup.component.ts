import { Component, Input, Output, EventEmitter, AfterContentInit, ContentChildren, QueryList, ElementRef, Renderer2 } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import Map from 'ol/Map';
//import { resourceLimits } from 'node:worker_threads';
import {ViewChild} from '@angular/core';
import { stopPropagation } from 'ol/events/Event';
import ImageLayer from 'ol/layer/Image';
import { ImageWMS } from 'ol/source';
//import { visitAll } from '@angular/compiler';
//Importaciones para la interacción en las mediciones de longitudes y áreas.
import Draw from 'ol/interaction/Draw';
import Overlay from 'ol/Overlay';
import View from 'ol/View';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import {LineString, Polygon, Geometry} from 'ol/geom';
import {getArea,getLength} from 'ol/sphere';
import {unByKey} from 'ol/Observable';
import Feature from 'ol/Feature';




export class Provincia {
    nombre!: string;
    codigo!: string;
}
export class Capa {
    nombre!: string;
    visibilidad!: boolean;
    opacidad!:number;
    borrar!: boolean;
    subir!:boolean;
    bajar!:boolean;
    idunique!:number;
    zindex!:number;
    zzindex!:number;
}
export class NuevaCapa
{
    extent:any=[];
    source!: ImageWMS;
 }

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
  /*  selector: 'popup',
    templateUrl: 'src/popup.html',
    styleUrls: ['src/popup.css']*/
})
/*export class PopupComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}*/
export class PopupComponent {

    Provincia: any = [];

    show = false;
    @ViewChild('bindingInput') bindingInput!: ElementRef;
    @ViewChild('ranger') ranger!: ElementRef;
    @ViewChild('valor') valor!:ElementRef;

    //@Input() title: string="hola";

    @Input()
    title!: any;

    mousemoveEvent: any;
    mouseupEvent: any;

    firstPopupX: number = 100;
    firstPopupY: number = 100;
    firstPopupZ: number = 3;
    xStep: number = 30;
    yStep: number = 30;
    zStep: number = 3;
    curX: number=0;
    curY: number=0;
    curZIndex: number=0;
    xStartElementPoint: number=0;
    yStartElementPoint: number=0;
    xStartMousePoint: number=0;
    yStartMousePoint: number=0;
    estevalor: any=[];
    estacapa: Capa[]=[];
    valorunique: number = 0;

    isMouseBtnOnPress: boolean=false;

    constructor(private elementRef: ElementRef,
        private renderer: Renderer2,
        public httpClient: HttpClient) {
        this.mouseup = this.unboundMouseup.bind(this);
        this.dragging = this.unboundDragging.bind(this);
    }
    sendGetrequest(){
        //this.httpClient.get('users')
        //this.httpClient.get('https://jsonplaceholder.typicode.com/users')
        //this.httpClient.get('https://sigdesaintranet2.mapama.gob.es/rest/Comun/ComunidadesAutonomas/1/Provincias')
        //this.httpClient.get('https://sigdesa.mapama.gob.es/WebServices/confvisorMulti/intranet/service.asmx?wsdl')
        this.httpClient.get<Provincia>('/rest/Comun/ComunidadesAutonomas/2/Provincias')
            .subscribe((res:{})=>{
                this.Provincia=res;
                console.log(res);
            }); 

    }
    Comunidades(){
        this.httpClient.get<Provincia>('https://sigdesaintranet2.mapama.gob.es/rest/Comun/ComunidadesAutonomas')
        .subscribe((res:{})=>{
            this.Provincia=res;
            console.log(res);
        }); 
    }
    Provincias(){
        this.httpClient.get<Provincia>('/rest/Comun/ComunidadesAutonomas/'+this.bindingInput.nativeElement.value+'/Provincias')
        .subscribe((res:{})=>{
            this.Provincia=res;
            console.log(res);
        });  
    }
    Municipios(){
        this.httpClient.get<Provincia>('/rest/Comun/Provincias/45/Municipios')
        .subscribe((res:{})=>{
            this.Provincia=res;
            console.log(res);
        });  
    }
   /* requestJsonp(url='https://archive.org/index.php?output=json',options,callback='callback'){
        const params=options.params.toString();
        return this.httpClient.jsonp(`${url}?${params}`,callback)
        .map((response: Response) => response[1])
        .map((results: any[]) => results.map((result: string)=>result[0]));

    }*/

    appear() {
        //prevent drag event 
        document.getSelection()?.removeAllRanges();
        this.setPos();
        var pepe=this.title.getLayers();
        this.estacapa=[];
        for(var i=1;i<this.title.getLayers().getLength();i++)
        {
            var pep=pepe.item(i)
            var pepito=pepe.item(i).getKeys();
            //this.estevalor.push(pepe.item(i).get('name'));
            this.estacapa.push({'nombre':pepe.item(i).get('name'),
                    'visibilidad':true,
                    'opacidad': 5,
                    'subir':true,
                    'bajar':true,
                    'borrar':true,
                    'idunique':i,
                    'zindex':this.title.getLayers().getLength()-i,
                    'zzindex':i
                    });
            //  var luz=pepito2['params_']['LAYERS'];
            //var texto=this.title.getLayers(1).get('name');
            //this.estevalor.push('hola');
           // this.title.getLayers().item(i).setOpacity(0.1*i);
        }
        this.estacapa.sort((a,b)=>a.idunique-b.idunique);

        //var ii=this.mymapId.getLayers().getLength();
        this.show = true;
    }
    stop(event: Event)
    {

        //event.preventDefault();
        event.stopPropagation();
    }
    OnVisible(event: Event,valor1:string,id:number){
        var p=event.target;
        this.title.getLayers().item(id).setOpacity(0);
        
// this.title.getLayers().item(id).setOpacity(Number(this.ranger.nativeElement.value)/100);
    }
    cambiavalor(number: number)
    {
        this.ranger.nativeElement.value+1;
    }
    getIndex(number1:number, valor:HTMLInputElement, val:EventTarget|null)
    {
        var dates = ['2003-05-17T00:00:00.000Z', '2013-02-01T00:00:00.000Z', '2017-08-03T00:00:00.000Z']
        if(!valor.checked)
            this.title.getLayers().item(number1).setOpacity(0);
        else
            this.title.getLayers().item(number1).setOpacity(100);
        //this.title.getLayers().item(number1).setOpacity(100);
        //window.setInterval(()=>{this.mostrarMapa(Number(this.ranger.nativeElement.value))},1000);
        /*setTimeout(() => {
            this.title.getLayers().item(number1).getSource().updateParams({'TIME':dates[2]});
        }, 2000);
        setTimeout(() => {
            this.title.getLayers().item(number1).getSource().updateParams({'TIME':dates[1]});
        }, 4000);
        setTimeout(() => {
            this.title.getLayers().item(number1).getSource().updateParams({'TIME':dates[0]});
        }, 8000);
        setTimeout(() => {
            this.title.getLayers().item(number1).getSource().updateParams({'TIME':dates[2]});
        }, 16000);*/
     /*   <input #valor type="checkbox" id="malex" name="genderx"
        (change)="OnVisible($event,valor.value,user.zzindex)"></td>*/

    }
    ChangeOpa(event: Event,valor:string,id:number){
        this.title.getLayers().item(id).setOpacity(Number(valor)/100);
// this.title.getLayers().item(id).setOpacity(Number(this.ranger.nativeElement.value)/100);
    }
    OnSubir(id:number)
    {
        //var zzindex=this.estacapa[id].zindex;
        
        var valorunique=this.estacapa.findIndex(x=>x.zzindex==id);//.find(x=>x.zindex==zzindex-1)?.zindex;
        //Intercambio de posiciones con un auxiliar.

        if(this.estacapa[valorunique].zzindex>=1)
        {
            var aux=this.estacapa[valorunique-1]
            aux.zzindex=aux.zzindex+1;
            aux.zindex=aux.zindex-1;
            this.estacapa[valorunique-1]=this.estacapa[valorunique];
            this.estacapa[valorunique-1].zzindex=id-1;
            this.estacapa[valorunique-1].zindex=this.estacapa[valorunique-1].zindex+1;
            this.estacapa[valorunique]=aux;
        }
        else
            console.log("Este elemento no puede subir más");
        //ordenamos el array por zindex y lo mostramos.
        //this.estacapa.sort((a,b)=>a.idunique-b.idunique);
        for(var i=1;i<this.title.getLayers().getLength();i++)
        {
            var valorunique=this.estacapa.findIndex(x=>x.idunique==i)
            this.title.getLayers().item(i).setZIndex(this.estacapa[valorunique].zindex);
        }
    }
    OnBajar(id:number)
    {
  
        var valorunique=this.estacapa.findIndex(x=>x.zzindex==id);//.find(x=>x.zindex==zzindex-1)?.zindex;
        //Intercambio de posiciones con un auxiliar.
        if(this.estacapa[valorunique].zzindex<=this.title.getLayers().getLength())
        {
            var aux=this.estacapa[valorunique+1]
            aux.zzindex=aux.zzindex-1;
            aux.zindex=aux.zindex+1;
            this.estacapa[valorunique+1]=this.estacapa[valorunique];
            this.estacapa[valorunique+1].zzindex=id+1;
            this.estacapa[valorunique+1].zindex=this.estacapa[valorunique].zindex-1;
            this.estacapa[valorunique]=aux;
        }
        else
            console.log("Este elemento no puede subir más");
        //ordenamos el array por zindex y lo mostramos.
        //this.estacapa.sort((a,b)=>a.idunique-b.idunique);
        for(var i=1;i<this.title.getLayers().getLength();i++)
        {
            var valorunique=this.estacapa.findIndex(x=>x.idunique==i)
            this.title.getLayers().item(i).setZIndex(this.estacapa[valorunique].zindex);
        }


    }
    OnBorrar(id:number)
    {
        this.title.removeLayer(this.title.getLayers().item(id));
        var valorunique=this.estacapa.findIndex(x=>x.idunique==id)
        this.estacapa.splice(valorunique,1);
    }
    OnAddService()
    {
/*        var nuevaCapa=new NuevaCapa();
        var nuevaFuente=new ImageWMS();
        var xi,yi,xd,yd;
        xi=-13884991;
        yi=-2870341;
        xd=7455066;
        yd=63382190;

        nuevaCapa.extent=[xi,yi,xd,yd]
        nuevaFuente.setUrl('http://88.12.28.231:8080/geoserver/topp/wms');
        nuevaFuente.updateParams({'LAYERS': 'states'});
        nuevaFuente.setProperties('');//ratio
        nuevaCapa.source=nuevaFuente;
        var url='http://88.12.28.231:8080/geoserver/topp/wms';
        var params: '{'*/

            var nuevalayers= new ImageLayer({
            extent:[-13884991, -2870341, 7455066, 63382190],
            source: new ImageWMS({
              url: 'http://88.12.28.231:8080/geoserver/topp/wms',
              params: {'LAYERS': 'states' },
              ratio:1,
              serverType:'geoserver',
            })
        });
        nuevalayers.setProperties({'name':'nueva capa', 'description':'xyz'});
        this.title.addLayer(nuevalayers);
        //Añadimos también a la lista array de capas.
        var i=this.title.getLayers().getLength();
        this.estacapa.push({'nombre':this.title.getLayers().item(i).get('name'),
        'visibilidad':true,
        'opacidad': 5,
        'subir':true,
        'bajar':true,
        'borrar':true,
        'idunique':i,
        'zindex':this.title.getLayers().getLength()-i,
        'zzindex':i
        });
        this.estacapa.sort((a,b)=>a.idunique-b.idunique);
        this.show = true;
    }


    MOpacidad(){
        this.title.getLayers().item(1).setOpacity(Number(this.bindingInput.nativeElement.value));
    }

    medirLongitud(){
        }
    medirAreas()
    {

    }

    disappear() {
      this.show = false;
    }
    minimizar(){
        this.show=false;
    }

    setPos() {
    /*    if (this.fatherPopup == undefined) {
            this.curX = this.firstPopupX;
            this.curY = this.firstPopupY;
            this.curZIndex = this.firstPopupZ;
        }
        else {
            this.curX = this.fatherPopup.curX + this.xStep;
            this.curY = this.fatherPopup.curY + this.yStep;
            this.curZIndex = this.fatherPopup.curZIndex + this.zStep;
        }*/
        this.curX = this.firstPopupX;
        this.curY = this.firstPopupY;
        this.curZIndex = this.firstPopupZ;
    }

    mouseup: (event: any) => void;
    unboundMouseup(event: any) {
        // Remove listeners
        this.mousemoveEvent();
        this.mouseupEvent();
    }
    
    mousedown(event: any) {
        if (event.button === 0/*only left mouse click*/) {
            this.xStartElementPoint = this.curX;
            this.yStartElementPoint = this.curY;
            this.xStartMousePoint = event.pageX;
            this.yStartMousePoint = event.pageY;

            // if listeners exist, first Remove listeners
            if (this.mousemoveEvent)
                this.mousemoveEvent();
            if (this.mouseupEvent)
                this.mouseupEvent();

            this.mousemoveEvent = this.renderer.listen("document", "mousemove", this.dragging);
            this.mouseupEvent = this.renderer.listen("document", "mouseup", this.mouseup);
        }
    }
    dragging: (event: any) => void;
    unboundDragging(event: any) {
        this.curX = this.xStartElementPoint + (event.pageX - this.xStartMousePoint);
        this.curY = this.yStartElementPoint + (event.pageY - this.yStartMousePoint);
    }
}



/*import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}*/
