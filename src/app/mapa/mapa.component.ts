import 'ol/ol.css';
import { Component, OnInit } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import Controlr from 'ol/control/Rotate';
import GeoJSON from 'ol/format/GeoJSON';
import VectorLayer from 'ol/layer/Vector';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';
import OSM from 'ol/source/OSM';
import XYZ from 'ol/source/XYZ';
import * as olProj from 'ol/proj';
//import TileLayer from 'ol/layer/Tile';
import Source from 'ol/source/WMSServerType';
import Layer from 'ol/layer/Layer';
import TileWMS from 'ol/source/TileWMS';
import ImageWMS from 'ol/source/ImageWMS';
import VectorSource from 'ol/source/Vector';
import {Image as ImageLayer, MapboxVector, Tile as TileLayer} from 'ol/layer';
import { CompileMetadataResolver } from '@angular/compiler';
import {Control, Zoom} from 'ol/control';
import {defaults as defaultControls,OverviewMap} from 'ol/control';
import {Rotate,FullScreen,MousePosition,ScaleLine,ZoomSlider,ZoomToExtent} from 'ol/control';
import {Attribution} from 'ol/control';
import { DragRotateAndZoom, defaults as defaultInteractions, Select} from 'ol/interaction';
import { BingMaps, Stamen} from'ol/source';
import { Coordinate } from 'ol/coordinate';
import { Pixel } from 'ol/pixel';
import { Stroke } from 'ol/style';
import {bbox as bboxStrategy} from 'ol/loadingstrategy'
import {Fill,Text, Circle} from 'ol/style';
import Feature, { FeatureLike } from 'ol/Feature';
import RenderFeature from 'ol/render/Feature';

import {PopupComponent} from './popup/popup.component'
//Incorporados para medir areas y longitudes.
import { Polygon, LineString, Geometry } from 'ol/geom';
import { MapBrowserEvent, Overlay } from 'ol';
import { getCenter } from 'ol/extent';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import {Draw} from 'ol/interaction';
import GeometryType from 'ol/geom/GeometryType';
import OverlayPositioning from 'ol/OverlayPositioning';
import {unByKey} from 'ol/Observable';
import {getArea, getLength} from 'ol/sphere';
import {DragBox} from 'ol/interaction'
import { platformModifierKeyOnly } from 'ol/events/condition';

//Incorporacion de filtros.
import { } from 'ol/format/filter/Comparison'
import { map } from 'rxjs/operators';




@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit { mapId!: string;
  map!: Map; map1!:Map;
 
  constructor() { }
//métodos de la clase.
pointerMoveHandler(evt: MapBrowserEvent)
{
  if (evt.dragging) {
    return;
  }
  var coordinate = evt.coordinate;    
  //container.innerText="En un lugar de la Mancha";
  //evt.map.getOverlayContainer().innerText="coordinate: "+coordinate;
  var container1!:HTMLElement;
  container1=document.getElementById('info2')!;
  container1.innerText="c::"+coordinate;
  evt.map.getOverlayById(1).setElement(container1);
  evt.map.getOverlayById(1).setPosition(coordinate);
 
}
mostrarCoordenadas(evt:MapBrowserEvent)
{
  if (evt.dragging) {
    return;
  }
  var coordinate = evt.coordinate;    
  //container.innerText="En un lugar de la Mancha";
  //evt.map.getOverlayContainer().innerText="coordinate: "+coordinate;
  var container1!:HTMLElement;
  container1=document.getElementById('info2')!;
  container1.innerText="c::"+coordinate;
  evt.map.getOverlayById(1).setElement(container1);
  evt.map.getOverlayById(1).setPosition(coordinate);
}


mostrarResultado()
{
  //container.innerText="En un lugar de la Mancha";
  //evt.map.getOverlayContainer().innerText="coordinate: "+coordinate;
  var container1!:HTMLElement;
  container1=document.getElementById('info2')!;
  container1.innerText="Resultado::";
  //valResultado.getOverlayById(1).setElement(container1);
  //mapa.map.getOverlayById(1).setPosition(coordinate);
}


createHelpTooltip(mapa: Map) {
  var helpTooltipElement!:HTMLElement;
  helpTooltipElement=document.getElementById('info2')!;
  if(helpTooltipElement)
  {
    helpTooltipElement.parentNode?.removeChild(helpTooltipElement);
  }
  helpTooltipElement=document.createElement('div');
  helpTooltipElement.className='ol-tooltip hidden';
  var helpTooltip=new Overlay({
    element: helpTooltipElement,
    offset: [15, 0],
  })
  helpTooltip.setPositioning(OverlayPositioning.CENTER_LEFT);
  mapa.addOverlay(helpTooltip);
}






  ngOnInit(): void {

    var cambio=true;
  var layer1 = new TileLayer({source: new OSM(),});
  var layerGeoServer2=new ImageWMS({
  url:'http://192.168.0.111:8080/geoserver/sf/wms',
  params: {'LAYERS' : 'streams'},
  ratio: 1,
  serverType: 'geoserver',
  });
  var layers = [
   /* new TileLayer({
     source: new OSM(),
    }),*/
   new ImageLayer({
    extent: [-13884991, 2870341, -7455066, 6338219],
    source: new ImageWMS({
     url: 'http://88.12.28.231:8080/geoserver/sf/wms',
     params: {'LAYERS': 'streams'},
     ratio: 1,
     serverType: 'geoserver',
    }),
  })];
 //Funciona 
/*var imagensourcelayer=new ImageWMS({
  url:'https://www.ign.es/wms-inspire/pnoa-ma?version=1.3.0'
  +'crs=EPSG:25830&dpiMode=7&format=image/png&layers=OI.OrthoimageCoverage',
  //url:'http://ovc.catastro.meh.es/Cartografia/WMS/ServidorWMS.aspx',
  //url:'https://wms.mapamagob.es/sig/Alimentacion/Paradores/wms.aspx?',
  //+'VERSION=1.3.0&SRS%3DEPSG:23029&bbox=511950,4662900,512150,4663100&width=756&height=756&transparent=No&layers=BU.Building',
  params:{'layers':'OI.OrthoimageCoverage'}
});
layers.push(new ImageLayer({
  extent: [-13884991, 2870341, -7455066, 6338219],
  source: imagensourcelayer,
}));*/

  layers.push(new ImageLayer({
    extent: [-13884991, 2870341, -7455066, 6338219],
    source: new ImageWMS({
    url: 'http://88.12.28.231:8080/geoserver/sf/wms',
    //url: 'https://wms.mapama.gob.es/sig/Alimentacion/Paradores/wms.aspx?'
    //      +'VERSION=1.3.0&REQUEST=GetCapabilities',
    //params: {'LAYER': 'BU.Building'},
    params: {'LAYERS': 'roads'},
    ratio: 1,
    serverType: 'geoserver',  
    })
  }));
  /*  layers.push(new ImageLayer({
      extent:[-13884991, -2870341, 7455066, 63382190],
      source: new ImageWMS({
        url: 'http://88.12.28.231:8080/geoserver/Prueba1/wms',
        params: {'LAYERS': 'comarcasagrarias' },
        ratio:1,
        serverType:'geoserver',
      })
  }));*/

  var style = new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0.6)',
    }),
    stroke: new Stroke({
      color: '#319FD3',
      width: 1,
    }),
    text: new Text({
      font: '12px Calibri,sans-serif',
      fill: new Fill({
        color: '#000',
      }),
      stroke: new Stroke({
        color: '#fff',
        width: 3,
      }),
    }),
  });

  var vectorSource1 = new VectorSource({
    format: new GeoJSON(),
    //url: 'http://88.12.28.231:8080/geoserver/Prueba/wfs',
    url: function (extent) {
      return (
        'http://88.12.28.231:8080/geoserver/Prueba/wfs?service=WFS&' +
        'version=1.1.0&request=GetFeature&typename=Prueba:comarcasagrarias&' +
        'outputFormat=application/json&srsname=EPSG:3857&' +
        'bbox=' +
        extent.join(',') +
        ',EPSG:3857'
        //',EPSG:4326'
      );
    },
    strategy: bboxStrategy,
  });
  var vector1 = new VectorLayer({
    source: vectorSource1,
    style: new Style({
      fill: new Fill({
        color: 'rgba(255, 255, 255, 0.6)',
      }),
      stroke: new Stroke({
        color: 'rgba(255, 0, 255, 1.0)',
        width: 2,
      }),
    }),
  });
  // Cobertura de puntos seleccionables.
  var vectorSource2 = new VectorSource({
    format: new GeoJSON(),
    //url: 'http://88.12.28.231:8080/geoserver/Prueba/wfs',
    url: function (extent) {
      return (
        'http://88.12.28.231:8080/geoserver/MAPA/wfs?service=WFS&' +
        'version=1.1.0&request=GetFeature&typename=MAPA:Bodegas&' +
        'outputFormat=application/json&srsname=EPSG:3857&' +
        'bbox=' +
        extent.join(',') +
        ',EPSG:3857'
        //',EPSG:4326'
      );
    },
    strategy: bboxStrategy,
    //strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
    //  maxZoom: 19}))
  });
  var mystyleseleccionold=new Style({
    image: new Circle({
      stroke: new Stroke({
        color: 'rgba(255, 0, 0, 1.0)',
        width: 2
       }),
      radius: 3
    })
  });
  var mystyleseleccion=new Style({
    image: new Circle({
      stroke: new Stroke({
        color: 'rgba(255, 255,255, 1.0)',
        width: 3
       }),
      radius: 3
    })
  });
  var vector2 = new VectorLayer({
    source: vectorSource2,
    style: new Style({
        image: new Circle({
          stroke: new Stroke({
            color: 'rgba(0, 0, 255, 1.0)',
            width: 2
           }),
          radius: 3
        })
      }),
  });

  //Cobertura de líneas seleccionables.
    // Cobertura de puntos seleccionables.
    var vectorSource3 = new VectorSource({
      format: new GeoJSON(),
      //url: 'http://88.12.28.231:8080/geoserver/Prueba/wfs',
      url: function (extent) {
        return (
          'http://88.12.28.231:8080/geoserver/MAPA/wfs?service=WFS&' +
          'version=1.1.0&request=GetFeature&typename=MAPA:pruebario&' +
          'outputFormat=application/json&srsname=EPSG:3857&' +
          'bbox=' +
          extent.join(',') +
          ',EPSG:3857'
          //',EPSG:4326'
        );
      },
      strategy: bboxStrategy,
      //strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
      //  maxZoom: 19}))
    });
    //Posible pater con imágenes.
    /*var miestilo=new Style();
    var tCnv = document.createElement("canvas");
    var tCtx = tCnv.getContext("2d");
    var cnv = document.createElement("canvas");
    var ctx = cnv.getContext("2d");
    var img=new Image();
    img.src = 'https://cdn1.iconfinder.com/data/icons/weather-429/64/weather_icons_color-02-256.png';
    var size = 320;
    tCnv.width = size;
    tCnv.height = size;
    tCtx?.rect(0,0,size,size);
    tCtx?.fill();
    tCtx?.drawImage(img, 0, 0, img.width, img.height, 0, 0, size, size);
  
    var pattern = ctx?.createPattern(tCnv, 'repeat');*/
    var lightStroke = new Style({
      stroke: new Stroke({
        color: [0, 0, 255, 0.6],
        width: 4,
        lineCap: 'square',
        lineDash: [20,20],
        lineDashOffset: 0
      })
    });
    
    var darkStroke = new Style({
      stroke: new Stroke({
        color: [255, 0, 0, 0.6],
        width: 4,
        lineCap: 'square',
        lineDash: [20,20],
        lineDashOffset: 20
      })
    });

    var vector3 = new VectorLayer({
      source: vectorSource3,
      style:[lightStroke,darkStroke]
      /*style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.6)',
        }),
        image: new Icon({opacity:1, size:[10,10], src:'assets/iisstart1.png',crossOrigin:'anonymous'}),
        stroke: new Stroke({
          color: 'rgba(255, 0, 0, 1.0)',
          //color: pattern!,
          lineDash:[10,20],
          //lineDashOffset:500,
          width: 10,
        }),
      }),*/
    });
  //custom source listener
  vector1.getSource().on('change',function(evt){
    if(cambio)
    {
        let info=document.getElementById('info1');
        if(info!==null)
          info.innerHTML="cargando wfs .... ";
          console.info('cargando wfs');
          cambio=false;
    }
    else
    {
      let info=document.getElementById('info1');
      if(info!==null)
        info.innerHTML="wfs cargando ";
        console.info('cargado wfs');
    }
  })
  vector1.getSource().clear();
  layers.push(new ImageLayer({
    extent:[-13884991, -2870341, 7455066, 63382190],
    source: new ImageWMS({
      url: 'http://88.12.28.231:8080/geoserver/topp/wms',
      params: {'LAYERS': 'states' },
      ratio:1,
      serverType:'geoserver',
    })
}));
  var layers2=[new TileLayer({
    source: new Stamen({
      layer:'terrain'
    })
  })];
   //Lo siguiente funciona correctamente para mostrar una imagen
   layers.push(new ImageLayer({
      extent: [-13884991, 2870341, -7455066, 6338219],
      source: new ImageWMS({
       url: 'http://88.12.28.231:8080/geoserver/nurc/wms',
        params: {'LAYERS': 'Img_Sample'},
        ratio: 1,
        serverType: 'geoserver',  
    })}));

    //Mapa de situación
    var overviewMapControl1=new OverviewMap({
      //className:'ol-overviewmap ol-custom-overviewmap',
      //target:'map',
      layers:[
        new TileLayer({
          source: new Stamen({
            layer:'terrain'
          })
        }),
        /*   new TileLayer({
          source: new XYZ({
            url:'http://tile.osm.org/{z}/{x}/{y}.png',
          })
        })*/
      ],
      tipLabel:'Mapa de situación',
      collapseLabel: '\u00BB',
      label: '\u00AB',
      collapsed: false,
    });
    var filtrocomarcas='co_ccaa=1 OR co_comarca=4507';
    const layercomarcas=new ImageLayer({
      extent:[-13884991, -2870341, 7455066, 63382190],
      source: new ImageWMS({
        url: 'http://88.12.28.231:8080/geoserver/Prueba1/wms',
        params: {'LAYERS': 'comarcasagrarias','CQL_FILTER':filtrocomarcas},
        ratio:1,
        serverType:'geoserver',
      })
  });
/*  const mifuente=new ImageWMS({url: 'http://88.12.28.231:8080/geoserver/Prueba1/wms?CQL_FILTER=',
  params: {'LAYERS': 'comarcasagrarias','CQL_FILTER':'ds_comarca=4507 and co_ccaa=2' },
  ratio:1,
  serverType:'geoserver',
});*/

  const layercomarcaswfs=new ImageLayer({
    extent:[-13884991, -2870341, 7455066, 63382190],
    source: new ImageWMS({
      url: 'http://88.12.28.231:8080/geoserver/Prueba/wfs',
      params: {'LAYERS': 'comarcasagrarias' },
      ratio:1,
      serverType:'geoserver',
    })
});

var container!:HTMLElement;
container=document.getElementById('info2')!;
container.innerText="En un lugar de la mancha";
var overlay=new Overlay({
  id:1,
  element: container,
  autoPan: true,
  autoPanAnimation:{
    duration: 250,
  },
});

//var l= overlay.getId();
    this.map = new Map({
      controls: [new Zoom()],//defaultControls().extend([ new OverviewMap()]),
      layers: [layer1,/*vectorvector1*/],
      //overlays:[overlay],
      target: 'hotel_map',
      view: new View({
       // center: [500000, 6000000],
       center: [-500000,4800000],
        zoom: 6
      })
    });
    this.map.addControl(overviewMapControl1);
    this.map.addControl(new ZoomToExtent());
    var scaleLine=new ScaleLine({
      className: 'miscala',
      minWidth: 200,
    });
    var mousePosition=new MousePosition({
      className: 'mimouseposition'
    });
    this.map.addControl(scaleLine);
    this.map.addControl(new FullScreen());
    this.map.addControl(mousePosition);

    //Botones personalizados.
    /*var options=opt_options || {};
    var button=document.createElement('button');
    button.innerHTML='N';
    var element=document.createElement('button');
    element.className='rotate-north ol-unselectable ol-control';
    element.appendChild(button);

    Control.call(this,{element:element,
              target:options.target,
            });*/
//Fin de botones personalizados.
    vector1.setZIndex(1);
    vector1.setProperties({'name':'vector de comarcas', 'description':'xyz'});
    var pepito=vector1.getProperties();
    this.map.addLayer(vector1);
    layercomarcas.setZIndex(22);
    layercomarcas.setOpacity(1);
    layercomarcas.setProperties({'name':'comarcas agrarias', 'description':'xyz'});
    var ppp=layercomarcas.get('name');
    this.map.addLayer(layercomarcas);
    for(var i=0;i<layers.length;i++)
    {
      layers[i].setZIndex(3+i);
      layers[i].setProperties({'name':'valor capa'+i, 'description':'xyz'});
      layers[i].setOpacity(0.5);
      this.map.addLayer(layers[i]);
    }
    vector2.setZIndex(9);
    vector2.setProperties({'name':'bodegas', 'description':'xyz'});
    this.map.addLayer(vector2);
    vector3.setZIndex(10);
    vector3.setProperties({'name':'caminos naturales', 'description':'xyz'});
    this.map.addLayer(vector3);
//Una capa más con los estados vía wms
var exampleLayer=new ImageLayer({
  source: new ImageWMS({
    url: 'http://88.12.28.231:8080/geoserver/topp/wms',
    //url: 'http://88.12.28.231:8080/geoserver/tiger/wms',
    //url: 'https://ahocevar.com/geoserver/wms',
    //url: 'https://wms.mapama.gob.es/sig/Alimentacion/Paradores/wms.aspx?',
    params:{
      'LAYERS':'topp:tasmania_cities',
      //'LAYERS':'tiger:poi',
      //'LAYERS':'topp:states',
      //'LAYERS' : 'BU.Building',//este es el de paradores.
      'STYLES':'ConImagenes',
      'SLD':'http://88.12.28.231:8080/geoserver/styles/ConImagenes.sld',
    },
    projection:'EPSG:4326',
    serverType: 'geoserver',
    crossOrigin:'anonymous',
  }) 
});
exampleLayer.setZIndex(12);
exampleLayer.setProperties({'name':'Paradores', 'description':'xyz'});
this.map.addLayer(exampleLayer);
var exampleLayer1=new ImageLayer({
  source: new ImageWMS({
   // url: 'https://www.ign.es/wms-inspire/pnoa-ma?version=1.3.0',
   url: 'https://www.ign.es/wms-inspire/pnoa-ma?',
   params:{
      'LAYERS' : 'OI.OrthoimageCoverage',
    },
    projection:'EPSG:4326',
    serverType: 'geoserver'
  }) 
});
exampleLayer1.setZIndex(13);
exampleLayer1.setProperties({'name':'imagen', 'description':'xyz'});
this.map.addLayer(exampleLayer1);

var exampleLayer2=new ImageLayer({
  source: new ImageWMS({
   // url: 'https://www.ign.es/wms-inspire/pnoa-ma?version=1.3.0',
   url: 'https://wms.mapama.gob.es/sig/Agricultura/CaractAgroClimaticas/wms.aspx?',
   params:{
      'LAYERS' : 'Temperatura media anual',
    },
    projection:'EPSG:4326',
    serverType: 'geoserver'
  }) 
});
exampleLayer2.setZIndex(14);
exampleLayer2.setProperties({'name':'papadakis', 'description':'xyz'});
this.map.addLayer(exampleLayer2);
//Capa para serie temporal.

var mosaico=new TileLayer({
  source: new TileWMS({
    url:'http://88.12.28.231:8080/geoserver/timeseries/wms',
    params:{'LAYERS':'timeseries:timeseries'}
  })
});
mosaico.setZIndex(15);
mosaico.setProperties({'name':'mosaico','description':'xyz'});
this.map.addLayer(mosaico);

//Fichero para incorporar nuevos elementos seleccionados
var vectorSourceSelected = new VectorSource();
var vectorSelected=new VectorLayer({
  source: vectorSource2,
  style:mystyleseleccionold,
});
vectorSelected.setZIndex(15);
vectorSelected.setProperties({'name':'circulos','description':'xyz'})
this.map.addLayer(vectorSelected);

//Añadir Overlay.
    this.map.addOverlay(overlay);
//Añadir interacciones.
// Mediciones.
var tipe=GeometryType.LINE_STRING;
//var tipe=GeometryType.POLYGON;

var sourcesal=new VectorSource();
    var draw=new Draw({
      type: tipe ,
      source: sourcesal,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: 'rgba(0, 0, 0, 0.5)',
          lineDash: [10, 10],
          width: 2,
        }),
        image: new Circle({
          radius: 5,
          stroke: new Stroke({
            color: 'rgba(0, 0, 0, 0.7)',
          }),
          fill: new Fill({
            color: 'rgba(255, 255, 255, 0.2)',
          }),
        }),
      }),
    });
   
  //  this.map.addInteraction(draw);
  /*  for(let i=this.map.getInteractions().getLength();i>=0;i--)
    {
      this.map.removeInteraction(this.map.getInteractions().item(i));
    }*/


   // tipe=GeometryType.POLYGON;
   tipe=GeometryType.LINE_STRING;
   var select=new Select();
   this.map.addInteraction(select);

   var selectedFeatures=select.getFeatures();


   var dragBox=new DragBox({
     condition: platformModifierKeyOnly,
   });
   this.map.addInteraction(dragBox);

   dragBox.on('boxend',function(){
     var mapa11=dragBox.getMap();
     var rotation=mapa11.getView().getRotation();
     var oblique=rotation % (Math.PI /2)!==0;
     var candidateFeatures=oblique ? [] : selectedFeatures;
     var extent=dragBox.getGeometry().getExtent();

    const selected: Feature<Geometry>[]= []; 
     var numero=mapa11.getLayers();
     vectorSource2.forEachFeature(function(feature){
       //console.log(feature);
     } );
     vectorSource2.forEachFeatureInExtent(extent,function(feature){
      console.log("Seleccionadas"+feature);
     });
     vectorSource2.forEachFeatureIntersectingExtent(extent,function(feature){
       feature.setStyle(mystyleseleccion);
        selected.push(feature);
     });
  
     for(let i=0;i<selected.length;i++)
     {
       var myfeatures=new Feature(selected[i] as Feature);
      // myfeatures.setStyle(mystyleseleccion);
       //selected.forEach((feature)=>feature.setStyle(myStyle));
       //vectorSourceSelected.addFeature(myfeatures);
       //vectorSourceSelected.removeFeature(my)
       myfeatures.setStyle(mystyleseleccion);
       vectorSource2.addFeature(myfeatures);
       console.log("Estamos grabando"+i);       
     }
    /* console.log("Número de elementos"+vectorSource2.getFeatures().length)
     var valorespares=0;
     vectorSource2.forEachFeature(function(feature){
        valorespares++;
        console.log(feature);
        if(valorespares>5000)
          feature.setStyle(mystyleseleccion);
     });*/
    });
   
   dragBox.on('boxstart',function(){
    selectedFeatures.clear();
   });
    //createMeasureTooltip;
 ///*this.createHelpTooltip;
 var sketck!: Feature;
 var output;
 draw.on('drawstart',function(event){
    var sketck2!:Geometry;
     sketck2=event.feature.getGeometry()!;

    var listener=sketck2.on('change',function(event){
    var geom=event.target;
    var output1;
    if(geom instanceof Polygon)
      output1=getArea(geom);
    else if (geom instanceof LineString)
      output1=getLength(geom);
      //alert(output1);
      var container1!:HTMLElement;
      container1=document.getElementById('info2')!;
      container1.innerText="Resultado::"+parseFloat((Number(output1)/1000).toString()).toFixed(2)+"km";
    });
  });
    
   // this.map.on('pointermove',this.pointerMoveHandler);
   //draw.on('drawend',this.mostrarResultado);
    draw.on('drawend',function(event){
     // alert("Buenos días");
     var sketck1!:Geometry;
     sketck1=event.feature.getGeometry()!;
     if(sketck1 instanceof Polygon)
     {
        output=getArea(sketck1);       
     }
     else
     {
        output=getLength(sketck1);
     }
     var container1!:HTMLElement;
     container1=document.getElementById('info2')!;
     container1.innerText="Resultado Final::"+parseFloat((Number(output)/1000).toString()).toFixed(2)+"km";
    });



//Fin de interacciones.

    //this.map.addLayer(vectorLayer1[0]);
    //this.map.getLayers().insertAt(1,layers[1]);
    //eliminar layers.
    /*this.map.removeLayer(this.map.getLayers().item(2));
      this.map.removeLayer(this.map.getLayers().item(2));
      this.map.removeLayer(this.map.getLayers().item(2));
      this.map.removeLayer(this.map.getLayers().item(2));
      this.map.removeLayer(this.map.getLayers().item(2));
      this.map.removeLayer(this.map.getLayers().item(2));*/
   // this.map.addLayer(vector);
   // this.map.addLayer(layers2[0]);
   //this.map.addLayer(layer1);
//Resaltar el recinto seleccionado.
var highlightStyle = new Style({
  stroke: new Stroke({
    color: '#f00',
    width: 1,
  }),
  fill: new Fill({
    color: 'rgba(255,0,0,0.1)',
  }),
  text: new Text({
    font: '12px Calibri,sans-serif',
    fill: new Fill({
      color: '#000',
    }),
    stroke: new Stroke({
      color: '#f00',
      width: 3,
    }),
  }),
});
var featureOverlay = new VectorLayer({
  source: new VectorSource(),
  map: this.map,
  style: function (feature) {
    highlightStyle.getText().setText(feature.get('name'));
    return highlightStyle;
  },
});

//Final de resalte.
var textoresult=['capas','capas','capas'];
var coleccion=this.map.getLayers();
var numcapa=0;
this.map.getLayers().forEach(element=>{
  //this.mapId=this.mapId+element.getOpacity().toString();
  //  this.mapId=this.mapId+element.getLayersArray();
  //  this.mapId=this.mapId+element.getProperties();
  element.setProperties({'idunique':numcapa})
  numcapa++;
})
/*for(i=0;i<this.map.getLayers().getLength();i++)
{
  var pepe=coleccion.item(i)
  if(i==5 || i==2 || i==4 || i==6 || i==7 )
  {
    var pepito=coleccion.item(i).getKeys();
    var pepito2=coleccion.item(i).get('source');
    var luz=pepito2['params_']['LAYERS'];
    var pepito3=pepito2.getProperties();
    var pepito1=pepito[8];
    textoresult.push(luz);
  }
  else
  {
    this.map.getLayers();
    textoresult.push(pepe.getClassName());
  }
}*/
var indice=textoresult.indexOf('comarcasagrarias');
textoresult.splice(indice,1);
textoresult.forEach(element => {
  this.mapId=this.mapId+element;
});
//this.mapId=textoresult:
//Trabajo con eventos sobre el mapa. 
//Vamos a intentar mostrar un popup sobre el mapa.
    
    var highlight: Feature | undefined=undefined;
//Evento movimiento sobre el mapa.
//Se debe comprobar el estado para realizar las diferetes operaciones.
//algunas de ellas simultaneamente.
//  - Mostrar coordenadas.
//  - Mostrar información subyacente.
//  - Herramienta de medición de longitudes y áreas.
//  - Herramienta de zoom de caja.
//  - pulsar y arrastrar.
//Medir longitudes.
var continuePolygonMsg = 'Click to continue drawing the polygon';
var continueLineMsg = 'Click to continue drawing the line';
var helpMsg="Click to strar drawing";
//
var helpTooltipElement!:HTMLElement;
var helpTooltip!: Overlay;
//this.map.on('pointermove',this.pointerMoveHandler);
//this.map.on('singleclick',this.mostrarCoordenadas);
    this.map.on('pointermove',function(evt){
      if(evt.dragging){
        return;
      }
 
      var pixel=evt.map.getEventPixel(evt.originalEvent);
      var feature = evt.map.forEachFeatureAtPixel(pixel, function(feature) {
        return feature;
     });
     if (feature) {
      console.log("Feature found");
      var info=document.getElementById('info');
      if(info!==null)
        info.innerHTML="seleccionado: "+ feature.getId() +":" +
        feature.get('ds_ccaa')+" : "+
        feature.get('ds_comarca')+" : " +
        feature.get('ds_provinc');

      }
      else
      {
      console.log("Elemento no encontrado");
      var info=document.getElementById('info');
      if(info!==null)
        info.innerHTML="No seleccionado";
      }
    });



    this.map.on('click', function(evt){
     /* var coordinate = evt.coordinate;    
      container.innerText="En un lugar de la Mancha";
      overlay.setPosition(coordinate);*/
    




      var pixel=evt.map.getEventPixel(evt.originalEvent);
      var feature = evt.map.forEachFeatureAtPixel(pixel, function(feature) {
                       return feature;
                    });
        if (feature) {
        console.log("Feature found");
        //layer1.setZIndex(9);
        //vector1.setZIndex(99);
      }
        else
        {
         // layer1.setZIndex(99);
         // vector1.setZIndex(9);
        console.log("Elemento no encontrado");
        }
        //evt.map.removeLayer(evt.map.getLayers().item(evt.map.getLayers().getLength()-1));
        //var j=-1;
   });


//asign the listeners on the source of tile layer
layer1.getSource().on('tileloadstart', function(event) {

    var info=document.getElementById('info1');
    if(info!==null)
    {
      if(cambio)
        info.innerHTML="cargando.... ";
        else
        info.innerHTML="cargando---cargando wfs"
    }

  });
  layer1.getSource().on('tileloadend',function(event)
  {
    var info=document.getElementById('info1');
    if(info!==null)
    {
      if(cambio)
        info.innerHTML="cargando.... ";
      else
        info.innerHTML="Tile cargado --- cargando wfs"
    }
      //info.innerHTML="cargado ";

  })
vectorSource1.set('loadstart', '');
vectorSource1.set('loadend', '');

vectorSource1.on('change:loadstart', function(evt){
    console.info('loadstart');
});
vectorSource1.on('change:loadend', function(evt){
    console.info('loadend');
});
  var listenerKey = vectorSource1.on('change', function(e) {
    if (vectorSource1.getState() == 'ready') {
      // hide loading icon
      // ...
      // and unregister the "change" listener 
      //ol.Observable.unByKey(listenerKey);
      // or vectorSource.unByKey(listenerKey) if
      // you don't use the current master branch
      // of ol3
      var info=document.getElementById('info1');
      if(info!==null)
        info.innerHTML="cargado wfs";
    }
    else
    {
      var info=document.getElementById('info1');
      if(info!==null)
        info.innerHTML="cargando wfs... ";
    }
  });


  
  
 /* osmLayer.getSource().on('tileloadend', function(event) {
  //replace with your custom action
  document.getElementById('tilesloaderindicatorimg').src = 'css/images/ok.png';
       });
  osmLayer.getSource().on('tileloaderror', function(event) {
  //replace with your custom action        
  document.getElementById('tilesloaderindicatorimg').src = 'css/images/no.png';
       });*/

//////Listener para wms
//and now asign the listeners on the source of it
/*var lyrSource = wmsLayer.getSource();
lyrSource.on('imageloadstart', function(event) {
console.log('imageloadstart event',event);
//replace with your custom action
var elemId = event.target.params_.ELEMENTID;
document.getElementById(elemId).src = 'css/images/tree_loading.gif'; 
});

lyrSource.on('imageloadend', function(event) {
 console.log('imageloadend event',event);
//replace with your custom action
var elemId = event.target.params_.ELEMENTID;
document.getElementById(elemId).src = 'css/images/ok.png'; 
});

lyrSource.on('imageloaderror', function(event) {
 console.log('imageloaderror event',event);
//replace with your custom action
var elemId = event.target.params_.ELEMENTID;
document.getElementById(elemId).src = 'css/images/no.png'; 
}); */       





    var overview = new OverviewMap({
      className:'ol-overviewmap ol-custom-overviewmap',
      layers:[
        new TileLayer({
          source: new XYZ({        
                url:'http://tile.osm.org/{z}/{x}/{y}.png',
        })
        })],
      collapseLabel: '\u00BB',
      label: '\u00AB',
      collapsed: true,
  });
    
  }

}
 