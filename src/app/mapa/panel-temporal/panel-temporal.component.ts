import { Component, Input, OnInit, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';
import {ViewChild} from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
//Trabajar con el mapa.
import Map from 'ol/Map';



@Component({
  selector: 'app-panel-temporal',
  templateUrl: './panel-temporal.component.html',
  styleUrls: ['./panel-temporal.component.css']
})
export class PanelTemporalComponent implements OnInit {


  show = false;
  @Output() numbers = new EventEmitter<{number : number}>();
  @ViewChild('ranger') ranger!: ElementRef;
  @Input()
  title!: any;
  intervalo!: number;

  mousemoveEvent: any;
  mouseupEvent: any;

  firstPopupX: number = 100;
  firstPopupY: number = 100;
  firstPopupZ: number = 3;
  xStep: number = 30;
  yStep: number = 30;
  zStep: number = 3;

  curX: number=0;
  curY: number=0;
  curZIndex: number=0;

  xStartElementPoint: number=0;
  yStartElementPoint: number=0;
  xStartMousePoint: number=0;
  yStartMousePoint: number=0;

  num!: Observable<number>;
  subscription!: Subscription;

  isMouseBtnOnPress: boolean=false;

  constructor(private renderer: Renderer2) {
    this.mouseup = this.unboundMouseup.bind(this);
    this.dragging = this.unboundDragging.bind(this);
  }

  ngOnInit(): void {
  }
  appear(){
    document.getSelection()?.removeAllRanges();
    this.setPos();
    this.show=true;

  }
  sendGetrequest()
  {

  }
  comenzar(valor:number){
    this.ranger.nativeElement.value=valor;
    this.intervalo=window.setInterval(()=>{this.mostrarMapa(Number(this.ranger.nativeElement.value))},1000);

    /*this.num = interval(1000);
    this.subscription = this.num.subscribe(n =>
      this.numbers.emit(n));*/
    console.log(this.ranger.nativeElement.value);
  }
  mostrarMapa(valor:number)
  {
      if(this.ranger.nativeElement.value>=100)
        this.ranger.nativeElement.value=0;
      else  
        this.ranger.nativeElement.value=valor+1;
      console.log(this.ranger.nativeElement.value+'valor'+valor);
  }

/*  stopCount() {
    this.subscription.unsubscribe();
  }*/


  stop(event: Event)
  {

      //event.preventDefault();
      event.stopPropagation();
  }
  OnParar()
  {
    if(this.intervalo)
      window.clearInterval(this.intervalo);
  }
  OnContinuar()
  {

  }
  OnAumentar()
  {

  }
  disappear() {
    if(this.intervalo)
      window.clearInterval(this.intervalo);
    this.show = false;
  }
  minimizar() {
    if(this.intervalo)
      window.clearInterval(this.intervalo);
    this.show = false;
  }


  setPos() {
        this.curX = this.firstPopupX;
        this.curY = this.firstPopupY;
        this.curZIndex = this.firstPopupZ;
    }

    mouseup: (event: any) => void;
    unboundMouseup(event: any) {
        // Remove listeners
        this.mousemoveEvent();
        this.mouseupEvent();
    }
    
    mousedown(event: any) {
        if (event.button === 0/*only left mouse click*/) {
            this.xStartElementPoint = this.curX;
            this.yStartElementPoint = this.curY;
            this.xStartMousePoint = event.pageX;
            this.yStartMousePoint = event.pageY;

            // if listeners exist, first Remove listeners
            if (this.mousemoveEvent)
                this.mousemoveEvent();
            if (this.mouseupEvent)
                this.mouseupEvent();

            this.mousemoveEvent = this.renderer.listen("document", "mousemove", this.dragging);
            this.mouseupEvent = this.renderer.listen("document", "mouseup", this.mouseup);
        }
    }
    dragging: (event: any) => void;
    unboundDragging(event: any) {
        this.curX = this.xStartElementPoint + (event.pageX - this.xStartMousePoint);
        this.curY = this.yStartElementPoint + (event.pageY - this.yStartMousePoint);
    }

}
