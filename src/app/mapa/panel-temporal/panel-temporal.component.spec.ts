import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelTemporalComponent } from './panel-temporal.component';

describe('PanelTemporalComponent', () => {
  let component: PanelTemporalComponent;
  let fixture: ComponentFixture<PanelTemporalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelTemporalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelTemporalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
