import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MapaComponent } from './mapa/mapa.component';
import { HttpClientModule } from '@angular/common/http';
import { PopupComponent } from './mapa/popup/popup.component';
import { PanelHComponent } from './panel-h/panel-h.component';
import { PanelVComponent } from './panel-v/panel-v.component';
import { PanelTemporalComponent } from './mapa/panel-temporal/panel-temporal.component';


@NgModule({
  declarations: [
    AppComponent,
    MapaComponent,
    PopupComponent,
    PanelHComponent,
    PanelVComponent,
    PanelTemporalComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
